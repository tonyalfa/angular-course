var express = require("express"),
  cors = require("cors");
var app = express();

app.use(express.json());
app.use(cors());
app.listen(3000, () => "Server running in port 3000");

var cities = [
  "Barcelona",
  "Barranquilla",
  "Puntarenas",
  "Fortuna",
  "Baldi",
  "Reventazon",
];
app.get("/cities", (req, res, next) =>
  res.json(
    cities.filter(
      (city) =>
        city.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1
    )
  )
);

var myDestinations = [
  {name: "Fortuna", imgUrl: "https://source.unsplash.com/featured/?{NATURE}", description: "", stars: 5}, 
  {name: "Volcan Poas", imgUrl: "https://source.unsplash.com/featured/?{FOREST}", description: "", stars: 5}, 
  {name: "Flamingo", imgUrl: "https://source.unsplash.com/featured/?{BEACH}", description: "", stars: 5}
];
app.get("/my", (req, res, next) => {
  res.json(myDestinations);
});
app.post("/my", (req, res, next) => {
  console.log(req.body);
  myDestinations.push(req.body);
  res.json(myDestinations);
});
