import { BrowserModule } from '@angular/platform-browser';
import {
  APP_INITIALIZER,
  Injectable,
  InjectionToken,
  NgModule,
} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DestinationAPI } from './models/destination-api.models';
import {
  HttpClient,
  HttpClientModule,
  HttpHeaders,
  HttpRequest,
} from '@angular/common/http';
import Dexie from 'dexie';

import { AppComponent } from './app.component';
import { WishedlistDestinationComponent } from './components/wishedlist-destination/wishedlist-destination.component';
import { WishedDestinationComponent } from './components/wished-destination/wished-destination.component';
import { DetailsDestinationComponent } from './components/details-destination/details-destination.component';
import { FormaddDestinationComponent } from './components/formadd-destination/formadd-destination.component';

import {
  DestinationState,
  DestinationReducer,
  initializeDestinationState,
  DestinationEffects,
  InitMyDataAction,
} from './models/destnation-state.model';
import {
  ActionReducerMap,
  Store,
  StoreModule as NgRxStoreModule,
} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { _runtimeChecksFactory } from '@ngrx/store/src/runtime_checks';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginUserComponent } from './components/login-user/login-user.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { AuthService } from './services/auth.service';
import { LoggedUserGuard } from './guards/logged-user/logged-user.guard';
import { FlightsComponent } from './components/flights/flights/flights.component';
import { FlightsHomeComponent } from './components/flights/flights-home/flights-home.component';
import { FlightsMoreInfoComponent } from './components/flights/flights-more-info/flights-more-info.component';
import { FlightsDetailsComponent } from './components/flights/flights-details/flights-details.component';
import { ReservationsModule } from './reservations/reservations.module';
import { Destination } from './models/destination.model';

export interface AppConfig {
  apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000',
};

export const APP_CONFIG = new InjectionToken<AppConfig>('api.config');

export const childrenFlightsRoutes: Routes = [
  { path: '', redirectTo: 'flights-home', pathMatch: 'full' },
  { path: 'flights-home', component: FlightsHomeComponent },
  { path: 'more-info', component: FlightsMoreInfoComponent },
  { path: ':id', component: FlightsDetailsComponent },
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: WishedlistDestinationComponent },
  { path: 'details', component: DetailsDestinationComponent },
  { path: 'login', component: LoginUserComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [LoggedUserGuard],
  },
  {
    path: 'flights',
    component: FlightsComponent,
    canActivate: [LoggedUserGuard],
    children: childrenFlightsRoutes,
  },
];

export interface AppState {
  destinations: DestinationState;
}

const reducers: ActionReducerMap<AppState> = {
  destinations: DestinationReducer,
};

let reducerInitialState = {
  destinations: initializeDestinationState(),
};

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.intializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) {}
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({
      'X-API-TOKEN': 'token-seguridad',
    });
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {
      headers: headers,
    });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// fin app init

// dexie db
@Injectable({
  providedIn: 'root',
})
export class MyDatabase extends Dexie {
  destinations: Dexie.Table<Destination, number>;
  //translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      destinations: '++id, name, imgUrl, description, stars',
    });
  }
}

export const db = new MyDatabase();
// fin dexie db

@NgModule({
  declarations: [
    AppComponent,
    WishedlistDestinationComponent,
    WishedDestinationComponent,
    DetailsDestinationComponent,
    FormaddDestinationComponent,
    LoginUserComponent,
    ProtectedComponent,
    FlightsComponent,
    FlightsHomeComponent,
    FlightsMoreInfoComponent,
    FlightsDetailsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducerInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      },
    }),
    EffectsModule.forRoot([DestinationEffects]),
    StoreDevtoolsModule.instrument(),
    ReservationsModule,
  ],
  providers: [
    DestinationAPI,
    AuthService,
    LoggedUserGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      deps: [AppLoadService],
      multi: true,
    },
    MyDatabase,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
