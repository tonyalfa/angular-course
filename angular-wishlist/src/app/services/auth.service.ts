import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  loginUser(userName: string, password: string): boolean{
    if(userName == "user" && password == "password") {
      localStorage.setItem("userName", userName);
      return true;
    }
    return false;
  }
  logoutUser(): any{
    localStorage.removeItem("userName");
  }
  getUser(): any{
    return localStorage.getItem("userName");
  }
  isLoggedIn(): boolean{
    return this.getUser() !== null;
  }
}
