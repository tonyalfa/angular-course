import { Destination } from './models/destination.model';

export const DESTINATIONS: Destination[] = [
  new Destination('Barcelona',
  '../assets/images/beach2.jpg',
  'Phasellus varius erat arcu, eu consectetur nisi hendrerit id. Aenean lobortis porttitor varius. Maecenas pulvinar turpis volutpat ipsum fermentum vehicula. Etiam metus tellus, efficitur at ipsum vitae, maximus malesuada augue. Aliquam aliquet, enim vitae mattis viverra, sem nisl semper arcu, ut efficitur turpis felis sed arcu. Nam euismod, odio id egestas ornare, neque dui tincidunt velit, vitae consectetur est magna non turpis. Suspendisse consequat tempus viverra',
  5),
  new Destination('Baldi',
  '../assets/images/forestpool.jpg',
  'Phasellus varius erat arcu, eu consectetur nisi hendrerit id. Aenean lobortis porttitor varius. Maecenas pulvinar turpis volutpat ipsum fermentum vehicula. Etiam metus tellus, efficitur at ipsum vitae, maximus malesuada augue. Aliquam aliquet, enim vitae mattis viverra, sem nisl semper arcu, ut efficitur turpis felis sed arcu. Nam euismod, odio id egestas ornare, neque dui tincidunt velit, vitae consectetur est magna non turpis. Suspendisse consequat tempus viverra',
  5),
  new Destination('Fiesta',
  '../assets/images/hotel.jpg',
  'Phasellus varius erat arcu, eu consectetur nisi hendrerit id. Aenean lobortis porttitor varius. Maecenas pulvinar turpis volutpat ipsum fermentum vehicula. Etiam metus tellus, efficitur at ipsum vitae, maximus malesuada augue. Aliquam aliquet, enim vitae mattis viverra, sem nisl semper arcu, ut efficitur turpis felis sed arcu. Nam euismod, odio id egestas ornare, neque dui tincidunt velit, vitae consectetur est magna non turpis. Suspendisse consequat tempus viverra',
  5),
  new Destination('Fortuna',
  '../assets/images/forest.jpg',
  'Phasellus varius erat arcu, eu consectetur nisi hendrerit id. Aenean lobortis porttitor varius. Maecenas pulvinar turpis volutpat ipsum fermentum vehicula. Etiam metus tellus, efficitur at ipsum vitae, maximus malesuada augue. Aliquam aliquet, enim vitae mattis viverra, sem nisl semper arcu, ut efficitur turpis felis sed arcu. Nam euismod, odio id egestas ornare, neque dui tincidunt velit, vitae consectetur est magna non turpis. Suspendisse consequat tempus viverra',
  5),
  new Destination('Flamingo',
  '../assets/images/beach.jpg',
  'Phasellus varius erat arcu, eu consectetur nisi hendrerit id. Aenean lobortis porttitor varius. Maecenas pulvinar turpis volutpat ipsum fermentum vehicula. Etiam metus tellus, efficitur at ipsum vitae, maximus malesuada augue. Aliquam aliquet, enim vitae mattis viverra, sem nisl semper arcu, ut efficitur turpis felis sed arcu. Nam euismod, odio id egestas ornare, neque dui tincidunt velit, vitae consectetur est magna non turpis. Suspendisse consequat tempus viverra',
  5),
];
