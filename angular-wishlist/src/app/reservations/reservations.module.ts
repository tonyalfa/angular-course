import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservationsRoutingModule } from './reservations-routing.module';
import { ReservationDetailsComponent } from './reservation-details/reservation-details.component';
import { ReservationListComponent } from './reservation-list/reservation-list.component';
import { ReservationsApiClientService } from './reservations-api-client.service';

@NgModule({
  declarations: [ReservationDetailsComponent, ReservationListComponent],
  imports: [
    CommonModule,
    ReservationsRoutingModule
  ],
  providers: [ReservationsApiClientService]
})
export class ReservationsModule { }
