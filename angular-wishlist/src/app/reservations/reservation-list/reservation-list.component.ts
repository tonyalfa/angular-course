import { Component, OnInit } from '@angular/core';
import { ReservationsApiClientService } from '../reservations-api-client.service';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {

  constructor(private api: ReservationsApiClientService) { }

  ngOnInit(): void {
  }

}
