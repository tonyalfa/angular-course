import { TestBed } from '@angular/core/testing';

import { ReservationsApiClientService } from './reservations-api-client.service';

describe('ReservationsApiClientService', () => {
  let service: ReservationsApiClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReservationsApiClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
