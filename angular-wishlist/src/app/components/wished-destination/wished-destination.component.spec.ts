import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WishedDestinationComponent } from './wished-destination.component';

describe('WishedDestinationComponent', () => {
  let component: WishedDestinationComponent;
  let fixture: ComponentFixture<WishedDestinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WishedDestinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WishedDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
