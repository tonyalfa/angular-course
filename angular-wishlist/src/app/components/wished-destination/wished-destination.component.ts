import {
  Component,
  OnInit,
  Input,
  HostBinding,
  EventEmitter,
  Output,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { Destination } from '../../models/destination.model';
import { ResetVotesAction, VoteDownAction, VoteUpAction } from '../../models/destnation-state.model';

@Component({
  selector: 'app-wished-destination',
  templateUrl: './wished-destination.component.html',
  styleUrls: ['./wished-destination.component.css'],
})
export class WishedDestinationComponent implements OnInit {
  @Input() destination: Destination;
  @Input() position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<Destination>;
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {}

  addPrefered() {
    this.clicked.emit(this.destination);
    return false;
  }
  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destination));
    return false;
  }
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destination));
    return false;
  }
  resetVotes(){
    this.store.dispatch(new ResetVotesAction(this.destination));
    return false;
  }
}
