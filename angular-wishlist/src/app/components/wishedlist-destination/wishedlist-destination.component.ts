import { Component, OnInit } from '@angular/core';
import { Destination } from '../../models/destination.model';

import { DestinationAPI } from '../../models/destination-api.models';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

@Component({
  selector: 'app-wishedlist-destination',
  templateUrl: './wishedlist-destination.component.html',
  styleUrls: ['./wishedlist-destination.component.css'],
})
export class WishedlistDestinationComponent implements OnInit {
  allDestinations: Destination[];
  updates: string[];

  constructor(
    private destinationAPI: DestinationAPI,
    private store: Store<AppState>
  ) {
    this.updates = [];
    this.store
      .select((state) => state.destinations.favorite)
      .subscribe((data) => {
        if (data != null) {
          this.updates.push(data.name + ' has been selected');
        }
      });
    this.store
      .select((state) => state.destinations.items)
      .subscribe((items) => this.allDestinations = items);
  }

  ngOnInit(): void {}

  addItem(destination: Destination) {
    this.destinationAPI.add(destination);
  }

  select(d: Destination) {
    this.destinationAPI.select(d);
  }
}
