import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WishedlistDestinationComponent } from './wishedlist-destination.component';

describe('WishedlistDestinationComponent', () => {
  let component: WishedlistDestinationComponent;
  let fixture: ComponentFixture<WishedlistDestinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WishedlistDestinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WishedlistDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
