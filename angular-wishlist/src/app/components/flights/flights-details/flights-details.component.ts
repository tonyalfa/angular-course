import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-flights-details',
  templateUrl: './flights-details.component.html',
  styleUrls: ['./flights-details.component.css'],
})
export class FlightsDetailsComponent implements OnInit {
  id: any;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe((params) => (this.id = params['id']));
  }

  ngOnInit(): void {}
}
