import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css'],
})
export class LoginUserComponent implements OnInit {
  messageError: string;
  constructor(public authService: AuthService) {
    this.messageError = '';
  }

  ngOnInit(): void {}

  login(name: string, password: string): boolean {
    this.messageError = '';
    if (!this.authService.loginUser(name, password)) {
      this.messageError = 'Incorrect credentials';
      setTimeout(
        function () {
          this.messageError = '';
        }.bind(this),
        2000
      );
    }
    return false;
  }
  logout(): boolean {
    this.authService.logoutUser();
    return false;
  }
}
