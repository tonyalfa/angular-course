import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Inject,
  forwardRef,
} from '@angular/core';
import { Destination } from '../../models/destination.model';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  ValidatorFn,
} from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import {
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-formadd-destination',
  templateUrl: './formadd-destination.component.html',
  styleUrls: ['./formadd-destination.component.css'],
})
export class FormaddDestinationComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Destination>;
  fg: FormGroup;
  minLong: number = 3;
  searchResults: string[];

  constructor(
    fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig
  ) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      name: [
        '',
        [
          Validators.required,
          this.nameValidator,
          this.nameValidatorParams(this.minLong),
        ],
      ],
      imgUrl: ['', Validators.required],
      description: [''],
    });

    this.fg.valueChanges.subscribe((form: any) => {
      //console.log('The form changed', form);
    });
    this.searchResults = [];
  }

  ngOnInit() {
    const nameInput = <HTMLInputElement>document.getElementById('name');
    fromEvent(nameInput, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter((text) => text.length > 2),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) =>
          ajax(this.config.apiEndpoint + '/cities?q=' + text)
        )
      )
      .subscribe(
        (ajaxResponse) => (this.searchResults = ajaxResponse.response)
      );
  }

  save(name: string, imgUrl: string, description: string): boolean {
    const destination = new Destination(
      name,
      'https://source.unsplash.com/featured/?{NATURE}',
      description,
      5
    );
    this.onItemAdded.emit(destination);
    return false;
  }

  nameValidator(control: FormControl): { [s: string]: boolean } {
    const inputLength = control.value.toString().trim().length;
    if (inputLength > 0 && inputLength < 5) {
      return { invalidName: true };
    }
    return null;
  }

  nameValidatorParams(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const inputLength = control.value.toString().trim().length;
      if (inputLength > 0 && inputLength < minLong) {
        return { minLongName: true };
      }
      return null;
    };
  }
}
