import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormaddDestinationComponent } from './formadd-destination.component';

describe('FormaddDestinationComponent', () => {
  let component: FormaddDestinationComponent;
  let fixture: ComponentFixture<FormaddDestinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormaddDestinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormaddDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
