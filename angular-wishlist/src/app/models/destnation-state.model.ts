import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Destination } from './destination.model';
import { DESTINATIONS } from '../mock-destinations';

export interface DestinationState {
  items: Destination[];
  loading: boolean;
  favorite: Destination;
}
export const initializeDestinationState = () => {
  return {
    items: [],
    loading: false,
    favorite: null,
  };
};

// ACTIONS
export enum ACTIONTYPES {
  NEW_DESTINATION = '[Destination] New',
  SELECT_FAVORITE = '[Destination] Favorite',
  VOTE_UP = '[Destination] Vote Up',
  VOTE_DOWN = '[Destination] Vote Down',
  RESET_VOTES = '[Destination] Reset Votes',
  INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}
export class NewDestinationAction implements Action {
  type = ACTIONTYPES.NEW_DESTINATION;
  constructor(public destination: Destination) {}
}
export class SelectFavoriteAction implements Action {
  type = ACTIONTYPES.SELECT_FAVORITE;
  constructor(public destination: Destination) {}
}
export class VoteUpAction implements Action {
  type = ACTIONTYPES.VOTE_UP;
  constructor(public destination: Destination) {}
}
export class VoteDownAction implements Action {
  type = ACTIONTYPES.VOTE_DOWN;
  constructor(public destination: Destination) {}
}
export class ResetVotesAction implements Action {
  type = ACTIONTYPES.RESET_VOTES;
  constructor(public destination: Destination) {}
}
export class InitMyDataAction implements Action {
  type = ACTIONTYPES.INIT_MY_DATA;
  constructor(public destinations: Destination[]) {}
}

export type DestinationActions =
  | NewDestinationAction
  | SelectFavoriteAction
  | VoteUpAction
  | VoteDownAction
  | ResetVotesAction
  | InitMyDataAction;

// REDUCERS
export function DestinationReducer(
  state: DestinationState,
  action: DestinationActions
): DestinationState {
  switch (action.type) {
    case ACTIONTYPES.NEW_DESTINATION: {
      return {
        ...state,
        items: [...state.items, (action as NewDestinationAction).destination],
      };
    }
    case ACTIONTYPES.SELECT_FAVORITE: {
      state.items.forEach((item) => item.setSelected(false));
      const fav: Destination = (action as SelectFavoriteAction).destination;
      fav.setSelected(true);
      return {
        ...state,
        favorite: fav,
      };
    }
    case ACTIONTYPES.VOTE_UP: {
      const voted: Destination = (action as VoteUpAction).destination;
      voted.voteUp();
      return {
        ...state,
      };
    }
    case ACTIONTYPES.VOTE_DOWN: {
      const voted: Destination = (action as VoteDownAction).destination;
      voted.voteDown();
      return {
        ...state,
      };
    }
    case ACTIONTYPES.RESET_VOTES: {
      const voted: Destination = (action as ResetVotesAction).destination;
      voted.resetVotes();
      return {
        ...state,
      };
    }
    case ACTIONTYPES.INIT_MY_DATA: {
      const destinations: Destination[] = (action as InitMyDataAction).destinations;
      return {
          ...state,
          items: destinations.map((d) => new Destination(d.name, d.imgUrl, d.description, d.stars))
        };
    }
  }
  return state;
}

// EFFECTS
@Injectable()
export class DestinationEffects {
  @Effect()
  newAdded$: Observable<Action> = this.actions$.pipe(
    ofType(ACTIONTYPES.NEW_DESTINATION),
    map(
      (action: NewDestinationAction) =>
        new SelectFavoriteAction(action.destination)
    )
  );
  constructor(private actions$: Actions) {}
}
