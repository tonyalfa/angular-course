import { Destination } from './destination.model';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import {
  NewDestinationAction,
  SelectFavoriteAction,
} from './destnation-state.model';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinationAPI {
  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {}

  add(destination: Destination) {
    const headers: HttpHeaders = new HttpHeaders({
      'X-API-TOKEN': 'token-seguridad',
    });
    const req = new HttpRequest(
      'POST',
      this.config.apiEndpoint + '/my',
      { destination },
      { headers: headers }
    );
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NewDestinationAction(destination));
        const myDb = db;
        myDb.destinations.add(destination);
        console.log('All destinations!');
        myDb.destinations.toArray().then(destination => console.log(destination))
      }
    });
  }
  select(destination: Destination) {
    this.store.dispatch(new SelectFavoriteAction(destination));
  }
}
