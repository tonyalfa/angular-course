import {v4 as uuid} from 'uuid';

export class Destination {
  private selected: boolean;
  public services: string[];
  public votes: number;
  public id = uuid();
  constructor(
    public name: string,
    public imgUrl: string,
    public description: string,
    public stars: number
  ) {
    this.services = ["Breakfast", "Tours"];
    this.votes = 0;
  }

  isSelected(): boolean {
    return this.selected;
  }
  setSelected(state: boolean): void {
    this.selected = state;
  }
  voteUp(){
    this.votes++;
  }
  voteDown(){
    this.votes--;
  }
  resetVotes(){
    this.votes = 0;
  }
}
